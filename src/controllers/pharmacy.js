const mongoose = require('mongoose');
const Pharmacy = mongoose.model('Pharmacy');

// list
exports.listPharmacys = async (req, res) => {
  try {
    const data = await Pharmacy.find({});
    res.status(200).send(data);
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
};

// create
exports.createPharmacy = async (req, res) => {
  try {
    const pharmacy = new Pharmacy({
      name: req.body.name,
      city: req.body.city,
      address: req.body.address,
      number: req.body.number, 
      telphone: req.body.telphone, 
      hour: req.body.hour,
    });

    console.log(pharmacy)

    await pharmacy.save();

    res.status(201).send({message: 'Usuário cadastrada com sucesso!'});
  } catch (e) {
    res.status(500).send({message: 'Falha ao cadastrar o usuário.'});
  }}

  // Delete

  exports.deletePharmacy = function (req, res) {
  

    Pharmacy.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};

  //Update

  exports.updatePharmacy = function (req, res) {
    Pharmacy.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
        if (err) return next(err);
        res.send('User udpated.');
    });
};
