const mongoose = require('mongoose');
const Medicines = mongoose.model('Medicines');

// list
exports.listMedicines = async (req, res) => {
  try {
    const data = await Medicines.find({});
    res.status(200).send(data);
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar '});
  }
};

// create
exports.createMedicine = async (req, res) => {
  try {
    const medicine = new Medicines({
      name: req.body.name,
      type: req.body.type,
      description: req.body.description,
      hour: req.body.hour,
      treatment: req.body.treatment,

    });

    console.log(medicine)

    await medicine.save();

    res.status(201).send({message: 'Usuário cadastrada com sucesso!'});
  } catch (e) {
    res.status(500).send({message: 'Falha ao cadastrar o usuário.'});
  }}

// Delete

exports.deleteMedicine = function (req, res) {
  

  Medicines.findByIdAndRemove(req.params.id, function (err) {
      if (err) return next(err);
      res.send('Deleted successfully!');
  })
};

//Update

exports.updateMedicine = function (req, res) {
  Medicines.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
      if (err) return next(err);
      res.send('User udpated.');
  });
};
