const mongoose = require('mongoose');
const { findOne } = require('../models/users');
const Users = mongoose.model('Users');

// list
exports.listUsers = async (req, res) => {
  try {
    const data = await Users.find({});
    res.status(200).send(data);
    
  } catch (e) {
    res.status(500).send({message: 'Falha ao carregar as menções.'});
  }
};

// create
exports.createUser = async (req, res) => {
  try {
    const user = new Users({
      name: req.body.name,
      age: req.body.age
    });

    console.log(user)

    await user.save();

    res.status(201).send({message: 'Usuário cadastrada com sucesso!'});
  } catch (e) {
    res.status(500).send({message: 'Falha ao cadastrar o usuário.'});
  }}

  // Delete

  exports.deleteUser = function (req, res) {
  

    Users.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};

  //Update

  exports.updateUser = function (req, res) {
    Users.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
        if (err) return next(err);
        res.send('User udpated.');
    });
};


