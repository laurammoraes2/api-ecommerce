const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


// App
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(bodyParser.json());

// Database



mongoose.connect('mongodb://localhost/api',  {
    useUnifiedTopology: true,
    useFindAndModify: true,
    useNewUrlParser: true,
    useCreateIndex: true 
}
);
const db = mongoose.connection;
  
db.on('connected', () => {
    console.log('Mongoose default connection is open');
});

db.on('error', err => {
    console.log(`Mongoose default connection has occured \n${err}`);
});

db.on('disconnected', () => {
    console.log('Mongoose default connection is disconnected');
   
});

process.on('SIGINT', () => {
    db.close(() => {
        console.log(
        'Mongoose default connection is disconnected due to application termination'
        );
        process.exit(0);
    });
});


// Load models
const Users = require('./models/users');
const Pharmacy = require('./models/pharmacy');
const Medicine = require('./models/medicines');


// Load routes
const indexRoutes = require('./routes/index-routes');
app.use('/', indexRoutes);



module.exports = app;