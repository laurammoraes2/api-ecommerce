const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  city: {
    type: String,
    required: true
  },
  address:{
      type: String, 
      required: true
  },
  number:{
      type: Number,
      required: true
  },
  telphone:{ 
      type: Number,
      required: true,
  },
  hour:{
      type: Number, 
      required: true
  }, 
});

module.exports = mongoose.model('Pharmacy', schema);