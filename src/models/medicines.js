const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  type: {
    type: String,
    required: true
  },
  description: {
      type: String,
      required: true
  }, 
  hour:{
      type: Number,
      required: true
  },
  treatment:{
      type: String, 
      required: true
  },
});

module.exports = mongoose.model('Medicines', schema);