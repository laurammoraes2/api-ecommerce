const express = require('express');
const router = express.Router();
const usersController = require('../controllers/users');
const pharmacysController = require('../controllers/pharmacy');
const medicinesController = require('../controllers/medicine');


router.get('/', (req, res, next) => {
  res.status(200).send({
    title: 'API',
   
  });
});

//Users
router.get('/users', usersController.listUsers);
router.post('/create-user', usersController.createUser);
router.put('/update-user/:id', usersController.updateUser);
router.delete('/delete-user/:id', usersController.deleteUser);

//Pharmacy
router.get('/pharmacys', pharmacysController.listPharmacys);
router.post('/create-pharmacy', pharmacysController.createPharmacy);
router.put('/update-pharmacy/:id', pharmacysController.updatePharmacy);
router.delete('/delete-pharmacy/:id', pharmacysController.deletePharmacy);

//Medicine
router.get('/medicines', medicinesController.listMedicines);
router.post('/create-medicine', medicinesController.createMedicine);
router.put('/update-medicine/:id', medicinesController.updateMedicine);
router.delete('/delete-medicine/:id', medicinesController.deleteMedicine);


module.exports = router;